# pamFoam - A multiphase, multiphysics CFD solver with coupled radiative transfer and biokinetics

`pamFoam` is a multiphase CFD solver which has coupled physical processes of the biochemical continuous reactor described by H&uuml;lsen et al. 2016, and for which the groundwork of the process kinetics was implemented by Puyol et al. 2017. This model currently allows for the simulation of mixed culture biomass in suspension and invokes the `photoBio` libraries which are an adaptation of the `radiation` libraries of the core OpenFOAM distribution. The `photoBio` libraries allow for extensible phase functions and multiple participating species, whether they absorb or scatter photons.

The model is implemented using the [OpenFOAM](https://openfoam.org) libaries for discretisation and solution of the governing equations.

## Getting Started

### Prerequisites

Install OpenFOAM and set up environment variables and paths. OpenFOAM runs natively on Linux systems, but can be run on any system. See [the Linux download instructions](https://openfoam.org/download/5-0-linux/) for help. If you are working on another platform, follow the links for the appropriate distribution.

Install ParaView.

### Installation

Build `pamFoam` by running Allwmake at the level of the parent directory:

`./Allwmake`

If that doesn't work, run the following commands first
`chmod +x Allwmake
./Allwmake`

## Running the Code

Setting up a case for the `pamFoam` solver proceeds similarly to typical `OpenFOAM` cases, with the exception of the additional configuration required by those models implemented specifically for `pamFoam`. It is assumed that the user is generally familiar with using `OpenFOAM` and setting up cases, so a detailed explanation will not be provided here.

### Initial and Boundary Conditions

To use the `pamFoam` solver, boundary and initial conditions must be specified on all additional fields that are solved, using appropriate files in the `0` subdirectory of the main case directory. The additional fields introduced by the `pamFoam` solver are: `SAC`, `SS`, `SH2`, `SIC`, `SIN`, `SIP`, `SI`, `XPB`, `XS`, `XI`, and the irradiance fields `G850` and `I`. The units of these state variables do vary. They can be looked up at line 18 of each file in the initial conditions. As a general rule, all species are in kg/m3 except for `SIC`, which is in mol/m3. The irradiance values are in W/m2, or kg/s3 (equivalent).

### PAM Configuration (biokineticsProperties dictionary)

A dictionary file called `biokineticsProperties` must be provided in the `constant` subdirectory to configure the constant simulation properties. This file requires a number of dictionaries and parameters to be defined, as outlined below.

The stoichiometry coefficients are defined as follows:

```
stoichiometry
{
    YPBPH        1.000000;
    YPBCH        0.680706;
    fSSXS        0.163824;
    fSACXS       0.166839;
    fSH2XS       8.442468E-02;
    fSINXS       0.011622;
    fSIPXS       0.002835;
    fSIXS        0.1518209;
    fXIXS        0.4330911;
    fNB          0.086000;
    fPB          0.015000;
    fSACCH       0.669100;
    fSH2CH       0.330900;
    fSICAU       1.000000;
    fINDEC       0.058000;
    fIPDEC       0.010000;
    YPBAU        YPBAU    [1 0 0 0 -1 0 0]  0.04032;
    fSICXS       fSICXS   [-1 0 0 0 1 0 0]  1.30397;
    fSH2AU       fSH2AU   [1 0 0 0 -1 0 0]  0.04032;
    fSICAC       fSICAC   [-1 0 0 0 1 0 0]  6.448413;
    fSICSS       fSICSS   [-1 0 0 0 1 0 0] -1.24276;
    fICDEC       fICDEC   [-1 0 0 0 1 0 0] -0.1984;
}

```

Note that one does not need to define units for the dimensionless stoichiometry values. The simulation will however crash if units are not defined for those values requiring units.


### Test Cases

All test cases are available in the [pamRun](https://gitlab.com/leboucher/pamRun) Git repository. Both cases used for the paper are in this repository. We will however not include the results in the repository, as this is a waste of space. Resuls will be dumped on UQ Espace at time of submission.

## Authors

- [Ed Barry](ed.m.barry@gmail.com)

## License

- This project is licensed under the GNU General Public License, version 3 (GPL-3.0). See LICENSE.txt for details.

## Acknowledgements

- The developers would like to thank the CRC for Water Sensitive Cities for the funding opportunity.
